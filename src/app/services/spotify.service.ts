import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  private http:HttpClient;

  constructor(http:HttpClient) {
    this.http = http; 

  }

  getQuery(query:string){
    const URL:string = `https://api.spotify.com/v1/${query}`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer QAJQz6xl3r6gpFdwGji_tiXkbhsXLJ2ZIYdx4cLdQ6-QW3nVjaVFli_j9rke8Ro7xJD6QrYTWUD4d5vqAA'
    });

    return this.http.get(URL, {headers});
  }

  getNewRelases(){
    return this.getQuery("browse/new-releases?limit=20").pipe( map(data => data['albums'].items));
  }

  gertArtists(text:string){
    return this.getQuery(`search?q=${ text }&type=artist&limit=20`).pipe(map(data=>data['artists'].items));
  }

  getArtist(idArtist:string){
    return this.getQuery(`artists/${idArtist}`);
  }

  getTopArtist(idArtist:string){
    return this.getQuery(`artists/${idArtist}/top-tracks?country=ES`).pipe(map(data=>data['tracks']));
  }
}
