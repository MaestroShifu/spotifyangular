import { Component, OnInit } from '@angular/core';

import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  private spotify:SpotifyService;
  private artists:any[];
  private loading:boolean;

  constructor(spotify:SpotifyService) { 
    this.spotify = spotify;
  }

  ngOnInit() {
  }

  search(text:string){
    this.loading = true;
    this.spotify.gertArtists(text).subscribe((data:any) => {
      this.loading = false;
      this.artists = data;
    });
  }

}
