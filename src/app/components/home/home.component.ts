import { Component, OnInit } from '@angular/core';


import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private spotify:SpotifyService;

  private loading:boolean;
  private error:boolean;
  private errorServicio:string;
  private songsNewRelase:any[];
  
  constructor(spotify:SpotifyService) {
    this.loading = true;
    this.error = false;
    this.spotify = spotify;

    this.spotify.getNewRelases().subscribe((data:any) => {
      this.songsNewRelase = data;
      this.loading = false;
    }, (errorServicio) => {
      
      this.errorServicio = errorServicio.error.error.message;

      this.error = true;
      this.loading = false;
    });
  }

  ngOnInit() {
  }

}
