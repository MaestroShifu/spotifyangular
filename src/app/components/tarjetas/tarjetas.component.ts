import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html',
  styleUrls: ['./tarjetas.component.css']
})
export class TarjetasComponent implements OnInit {

  @Input() items:any[];

  private router:Router;

  constructor(router:Router) { 
    this.router = router;
  }

  ngOnInit() {
  }

  getArtist(item:any){
    let artistId;

    if(item.type === "artist"){
      artistId = item.id;
    }else if(item.type === "album"){
      artistId = item.artists[0].id;
    }

    this.router.navigate(['/artist', artistId]);
  }

}
