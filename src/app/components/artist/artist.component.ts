import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {

  private router:ActivatedRoute;
  private spotify:SpotifyService;

  private artist:any = {};
  private tracks:any;

  private loadingArtist:boolean;
  private loadingTracks:boolean;

  constructor(router:ActivatedRoute, spotify:SpotifyService) { 
    this.loadingArtist = true;
    this.loadingTracks = true;
    this.router = router;
    this.spotify = spotify;

    this.router.params.subscribe(params => {

      this.getArtist(params.id);
      this.getTopArtist(params.id);
    });
  }

  ngOnInit() {
  }

  getArtist(idArtist:string){
    this.spotify.getArtist(idArtist).subscribe(artist => {
      this.loadingArtist = false;
      this.artist = artist;
    });
  }

  getTopArtist(idArtist:string){
    this.spotify.getTopArtist(idArtist).subscribe(artistTop => {
      this.loadingTracks = false;
      this.tracks = artistTop;

      console.log(artistTop);
    });
  }

}
